﻿clear
$path = "C:\Users\CARDENAS MEZA\Documents\Scripts\"
$servers = @("S1","S2","S3","S4")
$webApp = @("OGA","GO","Register")

$totalOGA = 0
$totalGO = 0
$totalRegister = 0

foreach($server in $servers)
{       $idxApp = 0
        foreach($app in $webApp)
        {
            $file = Import-Csv -Path ($path + $server + '\' + $app + '\logquey.csv') -Delimiter ' ' -Header A,B,C
            foreach ($line in $file)
            {
               if($idxApp -eq 0)
               {
                $totalOGA += $line.C #| out-host;
               }
               elseif($idxApp -eq 1)
               {  $totalGO += $line.C}
               else
               {  $totalRegister += $line.C }

            }

            $idxApp++
        }
}

echo $totalOGA
echo $totalGO
echo $totalRegister